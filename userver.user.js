// ==UserScript==
// @name         uServer
// @description  Вспомогательная панель саппортера
// @namespace    userver-dma-masterwind
// @author       dma, masterwind
// @version      19.9.30
// @updateURL    https://userver.netlify.com/userver.user.js
// @downloadURL  https://userver.netlify.com/userver.user.js
// @supportURL   https://bitbucket.org/masterwind/userver/issues
// @include      *
// @require      https://utils-masterwind.netlify.com/utils.js
// @grant        none
// @noframes
// ==/UserScript==


((stp, userver, template ) => {
	'use strict';
stp.debug && console.clear();
stp.debug && console.log(`window.utils`, window.utils );
	if (!window.utils ) return false;

stp.debug && console.log(`/admin\.cgi/.test(location.pathname)`, /admin\.cgi/.test(location.pathname) );
	if (/admin\.cgi/.test(location.pathname) ) return false;

stp.debug && console.log(`!/pdf/.test(document.contentType)`, !/pdf/.test(document.contentType) );
	!/pdf/.test(document.contentType)
		&& userver.getOwnerData() // get owner data
		&& utils.css.setLink('//mystifying-payne-74fc5e.netlify.com/userver.css') // set styles
	;
})(
	window.stp = {
		// debug     : 1,
		position  : localStorage.userver_sidebar_position || 'right',  // left | right - TODO toggle position
		whoisLink : 'https://whois.vu/?q=',
		searchers : [
			{target:'_blank', url:'https://www.google.ru/search?q={{domain}}',                        owntitle:'в Google',                                                      },
			{target:'_blank', url:'https://yandex.ru/search/?text={{domain}}',                        owntitle:'в Yandex',                                                      },
			{target:'_blank', url:'https://webcache.googleusercontent.com/search?q=cache:{{domain}}', owntitle:'в кеше Google',                                                 },
			{target:'_blank', url:'https://web.archive.org/web/*/{{domain}}',                         owntitle:'в Вебархиве',                                                   },
			{target:'_blank', url:'https://yandex.ru/infected?url={{domain}}',                        owntitle:'в Я.Malware',                                                   },
			{target:'_blank', url:'https://www.google.ru/safebrowsing/diagnostic?site={{domain}}',    owntitle:'в G.safebrowsing',                                              },
			{                 url:'javascript:;',                                                     owntitle:'whois',                  onclick:"userver.whoisIframe()",       },
			{target:'_blank', url:'https://virustotal.com/ru/domain/{{domain}}/information/',         owntitle:'VirusTotal domain info',                                        },
			{target:'_blank', url:'https://virustotal.com/ru/url/submission/?url={{domain}}',         owntitle:'VirusTotal site check',                                         },
			{target:'_blank', url:'{{findModerListingURL}}',                                          owntitle:'Письма в саппорт',       title:'Найти все обращения в саппорт', },
		],
	},
	window.userver = {
		// whois frame
		whoisIframe : (href = stp.whoisLink + userver.OD.domain ) => $.fancybox && $.fancybox({type : "iframe", href : href, wrapCSS : 'sptool-whois' }) || window.open(href),
		// init userver
		init : OD => {
stp.debug && console.log(`userver.init::OD`, OD );
			// check if domain
			(OD.domain = location.host == OD.chost ? OD.chost : location.host);
			// set link url to moder dialog
			(OD.user = utils.convert.site2user(OD.chost)) && (OD.dom = OD.user[0])
				&& (OD.findModerListingURL = `http://${OD.shost}/panel/?a=moders&l=f&q=${OD.user.slice(1)}&t=0&dom=${OD.dom}`);
			// check if new CP
			OD.newcp = OD.cp == 2;
			// update expanded status
			OD.expanded = +localStorage.sptool_open && 'expanded' || '';
			// update position
			OD.position = stp.position;
			// prepare data
			[OD.shop, OD.forum ] = [!!OD.servs.shop, !!OD.servs.forum ];

			// set uServer sidebar & widget
			template.setUserver(OD);
			// set find-In-aLog link
			!!document.querySelector('#uerror.blocked') && document.querySelector('#error-descr').insertAdjacentHTML('afterbegin',
				`<a class="alogsp" href="http://alog.ucozmedia.com/inner/?q=${OD.chost}">Найти сайт в Action Log</a>`);
			// cookie policy repadding
			window.cookie_policy && cookie_policy.classList.add('repadded');
			// set owner data
			userver.OD = OD;
		},
		// get owner data
		getOwnerData : () => fetch && fetch(location.origin + '/index/showmeowner', {method:"POST", })
			.then(r => r.ok && r.json() || console.log(r), e => console.log(e))
			// init userver
			.then((data = {}) => data.server && (data.shost = data.server + ({uweb : '.uweb.ru', }[data.cluster] || '.ucoz.net')) && userver.init(data))
			.catch(error => console.log(error)),
		togglePosition : (
			OD = userver.OD,
			POS = OD.position = stp.position = {left : 'right', right : 'left', }[localStorage.userver_sidebar_position || stp.position]
		) => {
			// update position
			localStorage.setItem('userver_sidebar_position', POS);
			// change widget position
			udraggable.className = OD.position;
		},
		toggleSidebar : (toggler, tClass = 'expanded') => {
			toggler.classList.toggle(tClass);
			sptools.classList.toggle(tClass);
			localStorage.setItem('sptool_open', +!+localStorage.sptool_open);
		},
	},
	window.template = {
		widget : `<div id=udraggable class="{{position}} u-hidden">
			<u onclick="userver.togglePosition();"></u>
			<a class="server-num" target="_blank" href="http://{{shost}}/cgi/uAdmin/admin.cgi?user={{chost}}&l=fu&r=0" title="Найти сайт в админке">{{server}}</a>
			<i id="panel" onclick="utils.copyToClipboard(this.textContent); this.style.color='blue';" title="Копировать адрес">{{chost}}</i>??vip=<b>VIP</b>??
			<a href="javascript:;" onclick="utils.copyToClipboard(this.dataset.email); this.style.color='blue';" title="Копировать email" class="owner-email" data-email="{{email}}"></a>
			??newcp=<span class="newcpflag">v2</span>??
			<i id="sptoggle" class="toggle-sidebar {{expanded}}" onclick="userver.toggleSidebar(this);"></i>
		</div>`,
		sidebar : `<div id=sptools class="u-menu myWinTD1 {{expanded}} u-hidden">
			<img src="https://logo.ucoz.com/LOGO.svg" width=32 alt="favicon" />
			<h3>Модули</h3>
			<ul>
				<li><a href="/">Главная</a>
				{{modules}}
			</ul>
			<h3>Cтраницы</h3>
			<ul>
				<li><a href="/index/${window.uCoz && '10">Выход с сайта' || '1">Страница входа'}</a>
				<li><a href="/index/8">Моя страница</a>
				<li><a href="/pda/">PDA</a>
				<li><a href="/index/53">Выход с PDA</a>
				??shop=<li><a href="/shop/invoices">Управление заказами</a>??
				<li><a target="_blank" href="/robots.txt">robots.txt</a>
				<li><a target="_blank" href="/sitemap.xml">sitemap.xml</a>
				??shop=<li><a target="_blank" href="/sitemap-shop.xml">sitemap-shop.xml</a>??
				??forum=<li><a target="_blank" href="/sitemap-forum.xml">sitemap-forum.xml</a>??
			</ul>
			<h3>Поиск сайта</h3>
			<ul>??searchers={{searchers}}??</ul>
		</div>`,
		button : '<li><a href={{url}} ??target=target={{target}}?? ??title=title="{{title}}"?? ??onclick=onclick="{{onclick}}"?? >{{owntitle}}</a>',
		setUserver : OD => {
			// prepare modules
			OD.modules = Object.keys(OD.servs).sort().map(module => !/mail|stat|feedsya|crosspost|feedsya|site|poll/.test(module)
				&& utils.template.render(template.button, OD.servs[module]) || undefined).join('');
			// prepare search links
			OD.searchers = stp.searchers.map(search => utils.template.render(template.button, search)).join('');
			// set content
			document.body.insertAdjacentHTML('afterbegin',
				// set widget
				utils.template.render(template.widget, OD)
				// set sidebar
				+ utils.template.render(utils.template.render(template.sidebar, OD), OD)
			)
		},
	}
);
